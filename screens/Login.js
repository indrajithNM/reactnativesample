import React, { useState } from "react";
import { View, StyleSheet, TextInput, Button } from "react-native";

const Login = (props) => {
  const [getUsername, setUsername] = useState("");
  const [getPassword, setPassword] = useState("");

  function updateUsername(username) {
    setUsername(username);
  }

  function updatePassword(password) {
    setPassword(password);
  }

  function login() {
    console.log(getUsername);
    console.log(getPassword);
    props.onSubmit(getUsername, getPassword);
  }

  function validate() {
    if (getUsername.length > 4 && getPassword.length > 6) {
      return true;
    } else {
      return false;
    }
  }

  return (
    <View style={styles.container}>
      <TextInput
        placeholder="Username"
        style={styles.input}
        onChangeText={updateUsername}
        value={getUsername}
      />
      <TextInput
        placeholder="Password"
        style={styles.input}
        onChangeText={updatePassword}
        value={getPassword}
        secureTextEntry={true}
      />
      <Button title="Let's Go" onPress={login} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    backgroundColor: "#f4f4f4",
    padding: 10,
    margin: 20,
    width: "80%",
    borderBottomColor: "black",
    borderBottomWidth: 1,
  },
});

export default Login;
