import React from "react";
import { View, StyleSheet, Text, Button } from "react-native";

const Home = (props) => {
  function reset() {
    props.onReset();
  }
  return (
    <View style={styles.container}>
      <Text>Username : {props.Username}</Text>
      <Button title="Go back" onPress={reset} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});

export default Home;
