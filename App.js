import React, { useState } from "react";
import { StyleSheet, SafeAreaView } from "react-native";
import Login from "./screens/Login";
import Home from "./screens/Home";

export default function App() {
  const [getUsername, setUsername] = useState();
  const [getPassword, setPassword] = useState();

  function userData(username, password) {
    setUsername(username);
    setPassword(password);
  }

  function userReset() {
    setUsername(false);
    content = <Login onSubmit={userData} />;
    console.log(getUsername);
  }

  let content = <Login onSubmit={userData} />;

  if (getUsername) {
    content = (
      <Home Username={getUsername} Password={getPassword} onReset={userReset} />
    );
  }
  if (!getUsername) {
    let content = <Login onSubmit={userData} />;
  }
  return <SafeAreaView style={styles.container}>{content}</SafeAreaView>;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
